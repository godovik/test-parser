import datetime

from models.date_start import DateStart


def formatted_date_to_datetime(date_string):
    return datetime.datetime.strptime(date_string, "%d.%m.%Y %H:%M:%S")


def normalise_date(date_string):
    return formatted_date_to_datetime(date_string).strftime("%Y-%m-%d %H:%M:%S")


def get_date_start():
    return DateStart.query.all()[0].date


def set_date_start(date_start):
    DateStart.query.all()[0].date = date_start