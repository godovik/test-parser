import urlparse

from bs4 import BeautifulSoup


class ContentExtractor(BeautifulSoup):

    not_found_str = "Not found"

    def __init__(self, base_url, *args, **kwargs):
        self.base_url = base_url
        super(ContentExtractor, self).__init__(*args, **kwargs)

    def get_first_element(self, css_selector):
        elements = self.find_all(css_selector)
        return elements[0] if elements else False

    def get_inner_html(self, css_selector):
        element = self.get_first_element(css_selector)
        return element.string if element else self.not_found_str

    def get_first_image(self):
        img = self.get_first_element("img")
        if not img:
            return self.not_found_str
        src = img.attrs['src']
        if src.startswith("http"):
            return src
        return urlparse.urljoin(self.base_url, src)
