import random
import shutil
import string
import threading

import requests

from models.site import Site
from utils.html_utils import ContentExtractor


def get_image_id():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))


def load_image(site):
    site.state = "3"
    response = requests.get(site.img, stream=True)
    local_filename = site.img.split('/')[-1]
    image_name = 'static/images/%s_%s' % (get_image_id(), local_filename)
    with open('/home/vagrant/Parser/%s' % image_name, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response
    site.local_img = "http://%s/%s" % ("192.168.50.4:5000", image_name)
    site.success = True
    site.state = "2"


def load_site(site_id):
    site = Site.query.get(site_id)
    site.state = "1"
    try:
        r = requests.get(site.url)
        ce = ContentExtractor(site.url, r.content, "html.parser")
        site.title = ce.get_inner_html("title")
        site.h1 = ce.get_inner_html("h1")
        site.img = ce.get_first_image()
        if site.img == ContentExtractor.not_found_str:
            site.success = True
            site.state = "2"
        else:
            load_image(site)
    except Exception, e:
        site.success = True
        site.error = str(e)
        site.state = "2"


def load_all_urls():
    for site in Site.all():
        threading.Thread(target=load_site, args=(site.id,)).start()
