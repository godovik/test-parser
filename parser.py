#!/usr/bin/env python
# -*- coding: utf-8 -*-

import atexit
import datetime
import json
import logging

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.date import DateTrigger
from flask import Flask, request

from database import db_session, init_db
from models.site import Site
from utils.downloads_utils import load_all_urls
from utils.datetime_utils import formatted_date_to_datetime, normalise_date, get_date_start, set_date_start

logging.basicConfig()
app = Flask(__name__)
scheduler = BackgroundScheduler()


@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/<path:resource_type>/<path:path>')
def static_resources(resource_type, path):
    return app.send_static_file('%s/%s' % (resource_type, path))


@app.route('/parsing/stop', methods=["POST"])
def stop_parsing():
    Site.delete()
    return "OK"


@app.route('/parsing/start', methods=["POST"])
def start_parsing():
    Site.delete()
    parsing_start = request.json['parsingStart']
    for site_data in request.json['urlList']:
        site = Site(
            url=site_data['url'],
            state="0")
        db_session.add(site)
    db_session.commit()
    if (formatted_date_to_datetime(parsing_start)) > datetime.datetime.now():
        set_date_start(formatted_date_to_datetime(parsing_start))
        scheduler.add_job(
            func=load_all_urls,
            trigger=DateTrigger(run_date=normalise_date(parsing_start)),
            id='load_all_urls',
            replace_existing=True)
    else:
        set_date_start(datetime.datetime.now())
        load_all_urls()
    return "OK"


@app.route("/parser/state")
def last_data():
    sites_count = Site.query.count()
    loaded_sites_count = Site.query.filter_by(success=True).count()
    return json.dumps({
        'sites_count': sites_count,
        'loaded_sites_count': loaded_sites_count,
        'parsing_run': sites_count > 0 and sites_count != loaded_sites_count,
        'parsing_start': get_date_start().strftime("%d.%m.%Y %H:%M:%S"),
        'parsing_state': [{
            'id': site.id,
            'url': site.url,
            '_url': site.url,
            'state': site.state,
            '_state': site.state,
            'error': site.error if site.error else False,
            'title': site.title if site.title else "",
            'h1': site.h1 if site.h1 else "",
            'img': site.img if site.img else "",
            'local_img': site.local_img if site.local_img else "",
        } for site in Site.all()]
    })


@app.route('/site/<int:site_id>', methods=["DELETE"])
def delete_site(site_id):
    Site.query.filter_by(id=site_id).delete()
    return "OK"


if __name__ == '__main__':
    scheduler.start()
    print "\nПарсер успешно запущен. Для доступа к нему перейдите по ссылке: http://192.168.50.4:5000/"
    init_db()
    app.run(host="192.168.50.4", port="5000")
    atexit.register(lambda: scheduler.shutdown())
