from datetime import datetime

from sqlalchemy import Column, Integer, DateTime

from database import Base, db_session


class DateStart(Base):
    __tablename__ = 'date_start'
    id = Column(Integer, primary_key=True)
    _date = Column('date', DateTime)

    def __init__(self, date=None):
        self.date = date or datetime.now()

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, new_date):
        self._date = new_date
        db_session.commit()
