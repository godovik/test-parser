from sqlalchemy import Column, Integer, String, Boolean

from database import Base, db_session


class Site(Base):
    __tablename__ = 'site'
    id = Column(Integer, primary_key=True)
    url = Column(String)
    _state = Column('state', String)
    title = Column(String)
    img = Column(String)
    local_img = Column(String)
    _error_message = Column('error_message', String)
    h1 = Column(String)
    success = Column(Boolean)

    def __init__(self, url=None, state=None):
        self.url = url
        self.state = state

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new_state):
        self._state = new_state
        db_session.commit()

    @property
    def error(self):
        return self._error_message

    @error.setter
    def error(self, error_message):
        self._error_message = error_message
        db_session.commit()

    @classmethod
    def delete(cls):
        cls.query.delete()

    @classmethod
    def all(cls):
        return cls.query.all()


