angular.module('app', []).controller('urlListController', function($scope) {
    $('.date').datetimepicker({
        format: 'DD.MM.YYYY HH:mm:ss'
    });
    $scope.urlList = [];
    $scope.parsingStart = "12";
    $scope.initInProgress = true;
    $scope.parsingInProgress = false;
    $scope.activePage = 0;
    $scope.isValidUrl = function(value) {
        return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
    };
    $scope.deleteUrl = function(urlIndex) {
        var urlData = $scope.urlList.splice(urlIndex, 1)[0];
        $.ajax({
            dataType: "json",
            method: "DELETE",
            url: ["/site", urlData.id].join("/"),
        });
    };
    $scope.addUrl = function() {
        $scope.urlList.push({});
    };
    $scope.stopParsing = function() {
        $scope.parsingInProgress = false;
        $.ajax({
            method: "POST",
            url: "/parsing/stop",
        });
    };
    $scope.getState = function(stateId) {
        return [
            "Wait parsing start",
            "Start load page",
            "Parsing Success",
            "Start load image"
        ][stateId];
    };
    $scope.getProgress = function(stateId) {
        return [
            "30%",
            "50%",
            "100%",
            "75%"
        ][stateId || 0] || "0%";
    };
    $scope.isStr = function() {
        for (var variableIndex in arguments) {
            if (typeof arguments[variableIndex] !== "string") return false;
        }
        return true;
    };
    $scope.isEditUrl = function(urlIndex) {
        if ($scope.urlList[urlIndex]['url'] != $scope.urlList[urlIndex]['_url']) {
            $scope.urlList[urlIndex]['state'] = 0;
            return true;
        }
        $scope.urlList[urlIndex]['state'] = $scope.urlList[urlIndex]['_state'];
        return false;
    };
    $scope.updateViewData = function(init) {
        if (!init && !$scope.parsingInProgress) {
            return;
        }
        $.ajax({
            dataType: "json",
            method: "GET",
            url: "/parser/state",
            success: function(data) {
                $scope.initInProgress = false;
                $scope.parsingStart = data['parsing_start'];
                $scope.parsingInProgress = data['parsing_run'];
                $scope.urlList = data['parsing_state'];
                $scope.$digest();
                if ($scope.parsingInProgress) {
                    setTimeout($scope.updateViewData, 100);
                }
            }
        });
    };
    $scope.isResultExist = function(urlIndex) {
        var urlData = $scope.urlList[urlIndex];
        return $scope.isStr(urlData['title'], urlData['h1'], urlData['img']) &&
            (urlData['title'].length || urlData['h1'].length || urlData['img'].length);
    };
    $scope.togglePage = function(pageNumber) {
        if ($scope.activePage == pageNumber) {
            return;
        }
        $scope.activePage = pageNumber;
        $(".page-link").removeClass("active");
        $(".page-link-" + pageNumber).addClass("active");
    };
    $scope.isHideSiteBlock = function(urlIndex) {
        if ($scope.initInProgress || $scope.isEditUrl(urlIndex)) {
            return true;
        }
        var hiddenCount = $scope.hiddenCount();
        if ($scope.activePage !== 0) {
            return urlIndex - hiddenCount < 3;
        } else {
            return urlIndex - hiddenCount >= 3
        }
    };
    $scope.hiddenCount = function() {
        var count = 0;
        $.each($scope.urlList, function(i, urlData) {
            if (urlData.state != 2) {
                count++;
            }
        });
        return count;
    };
    $scope.showingCount = function() {
        var count = $scope.urlList.length - $scope.hiddenCount();
        if (count <= 3) {
            $scope.togglePage(0);
        }
        return count;
    };
    $scope.isHidePagination = function() {
        if ($scope.urlList.length <= 3) {
            return true;
        }
        return $scope.showingCount() <= 3;
    };
    $scope.startParsing = function() {
        $scope.parsingStart = $("#startDateInput").val();
        $.ajax({
            method: "POST",
            url: "/parsing/start",
            data: JSON.stringify({
                urlList: $scope.urlList,
                parsingStart: $scope.parsingStart
            }),
            dataType: 'json',
            contentType: 'application/json'
        }).always(function() {
            $(".progress-bar").css("width", '30%');
            $scope.parsingInProgress = true;
            $scope.updateViewData();
            $scope.$digest();
        });
    };
    $scope.updateViewData(true);
});
